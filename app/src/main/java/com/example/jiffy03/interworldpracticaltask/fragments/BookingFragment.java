//package com.example.jiffy03.interworldpracticaltask.fragments;
//
//import android.location.Location;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.Toast;
//
//import com.example.jiffy03.interworldpracticaltask.MainActivity;
//import com.example.jiffy03.interworldpracticaltask.R;
//import com.example.jiffy03.interworldpracticaltask.interfaces.UpdateBookingInterface;
//import com.example.jiffy03.interworldpracticaltask.model.BookingModel;
//import com.example.jiffy03.interworldpracticaltask.model.LoginResponse;
//import com.example.jiffy03.interworldpracticaltask.serverCalls.RestClientRequests;
//
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Locale;
//
///**
// * Created by Sonam gupta on 16-12-2017.
// */
//
//public class BookingFragment extends RootFragment {
//
////    Calendar myCalendar = Calendar.getInstance();
////    private EditText etBookingDate, etBookingTime, etMessage;
////    private Button btnDone;
////    private RadioGroup radioGroup;
////    private RadioButton radioButton;
//
//
//    @Nullable
////    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View rootView = inflater.inflate(R.layout.booking_fragment_layout, container, false);
//
//
//        return rootView;
//    }
//
//    public void addBookingDate(final String type) {
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//        String formattedDate = sdf.format(c.getTime());
//        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss", Locale.US);
//        String formattedTime = sdfTime.format(c.getTime());
//
//        BookingModel bookingModel = new BookingModel();
//        bookingModel.setBook_time(formattedTime);
//        bookingModel.setBookDate(formattedDate);
//        bookingModel.setMessage("Test message");
//        bookingModel.setToken(loginResponse.getData().getToken());
//        bookingModel.setLatitude(loginResponse.getData().getLocation().get(0).getLatitude());
//        bookingModel.setLongitude(loginResponse.getData().getLocation().get(0).getLongitude());
//        bookingModel.setCheckInType(type);
//        RestClientRequests.saveBooking(bookingModel, new UpdateBookingInterface() {
//            @Override
//            public void updateBooking(boolean success) {
//                if (success)
//                    Toast.makeText(getActivity(), type +" Data updated", Toast.LENGTH_LONG).show();
//                else
//                    Toast.makeText(getActivity(), "error in update data", Toast.LENGTH_LONG).show();
//
//
//            }
//        });
//    }
//
////    public void initViews(final View rootView) {
////        btnDone = rootView.findViewById(R.id.btn_done);
////        btnDone.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if (TextUtils.isEmpty(etBookingDate.getText())) {
////                    Toast.makeText(getActivity(), "Please enter booking date", Toast.LENGTH_LONG).show();
////                } else if (TextUtils.isEmpty(etBookingTime.getText())) {
////                    Toast.makeText(getActivity(), "Please enter booking time", Toast.LENGTH_LONG).show();
////                } else if (TextUtils.isEmpty(etMessage.getText())) {
////                    Toast.makeText(getActivity(), "Please enter message", Toast.LENGTH_LONG).show();
////                } else {
////                    BookingModel bookingModel = new BookingModel();
////                    bookingModel.setBook_time(etBookingTime.getText().toString());
////                    bookingModel.setBookDate(etBookingDate.getText().toString());
////                    bookingModel.setMessage(etMessage.getText().toString());
////                    bookingModel.setToken(((MainActivity) getActivity()).getLoginData().getData().getToken());
////                    bookingModel.setLatitude(((MainActivity) getActivity()).getLoginData().getData().getLocation().get(0).getLatitude());
////                    bookingModel.setLongitude(((MainActivity) getActivity()).getLoginData().getData().getLocation().get(0).getLongitude());
////                    int selectedId = radioGroup.getCheckedRadioButtonId();
////
////                    // find the radiobutton by returned id
////                    radioButton = (RadioButton) rootView.findViewById(selectedId);
////                    bookingModel.setCheckInType(radioButton.getText().toString());
////                    RestClientRequests.saveBooking(bookingModel, new UpdateBookingInterface() {
////                        @Override
////                        public void updateBooking(boolean success) {
////                            if (success)
////                                Toast.makeText(getActivity(), "Data updated", Toast.LENGTH_LONG).show();
////                            else
////                                Toast.makeText(getActivity(), "error in update data", Toast.LENGTH_LONG).show();
////
////
////                        }
////                    });
////                }
////
////            }
////        });
////        etBookingDate = rootView.findViewById(R.id.et_booking_date);
////        etBookingTime = rootView.findViewById(R.id.et_booking_time);
////        etMessage = rootView.findViewById(R.id.et_message);
////        radioGroup = (RadioGroup) rootView.findViewById(R.id.radio);
////        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
////
////            @Override
////            public void onDateSet(DatePicker view, int year, int monthOfYear,
////                                  int dayOfMonth) {
////                // TODO Auto-generated method stub
////                myCalendar.set(Calendar.YEAR, year);
////                myCalendar.set(Calendar.MONTH, monthOfYear);
////                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
////                updateLabel();
////            }
////
////        };
////        etBookingDate.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                new DatePickerDialog(getActivity(), date, myCalendar
////                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
////                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
////            }
////        });
////        final TimePickerWithSecond mTimePicker = new TimePickerWithSecond(getActivity(), new TimePickerWithSecond.OnTimeSetListener() {
////
////            @Override
////            public void onTimeSet(TimePicker view, int hourOfDay, int minute, int seconds) {
////                etBookingTime.setText(String.format("%02d", hourOfDay) +
////                        ":" + String.format("%02d", minute) +
////                        ":" + String.format("%02d", seconds));
////            }
////
////        }, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), myCalendar.get(Calendar.SECOND), true);
////        etBookingTime.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                mTimePicker.show();
////            }
////        });
////
////
////    }
//
////    @Override
////    public void onRefreshData() {
////
////    }
////
////    @Override
////    public boolean onBackPressed() {
////        return false;
////    }
//
////    /*set selected date  on edit text
////  * */
////    private void updateLabel() {
////        String myFormat = "yyyy-MM-dd"; //In which you need put here
////        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
////
////        etBookingDate.setText(sdf.format(myCalendar.getTime()));
////    }
//
//
//    @Override
//    public void onRefreshData() {
//
//    }
//
//    @Override
//    public boolean onBackPressed() {
//        return false;
//    }
//}
