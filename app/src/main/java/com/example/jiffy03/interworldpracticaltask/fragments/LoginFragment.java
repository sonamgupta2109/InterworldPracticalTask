package com.example.jiffy03.interworldpracticaltask.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jiffy03.interworldpracticaltask.MainActivity;
import com.example.jiffy03.interworldpracticaltask.R;
import com.example.jiffy03.interworldpracticaltask.interfaces.LoginResultInterface;
import com.example.jiffy03.interworldpracticaltask.model.LoginResponse;
import com.example.jiffy03.interworldpracticaltask.model.LoginUser;
import com.example.jiffy03.interworldpracticaltask.serverCalls.RestClientRequests;

/**
 * Created by Sonam gupta on 16-12-2017.
 */

public class LoginFragment extends RootFragment {

    private Button btnLogin;
    private EditText etPassword, etEmail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.login_fragment, container, false);
        initViews(rootView);

        return rootView;
    }

    public void initViews(View rootView) {
        btnLogin = rootView.findViewById(R.id.login_btn);
        etPassword = rootView.findViewById(R.id.et_password_login);
        etEmail = rootView.findViewById(R.id.et_email_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(etEmail.getText())) {
                    Toast.makeText(getActivity(), "Please enter Email", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(etPassword.getText())) {
                    Toast.makeText(getActivity(), "Please enter Password", Toast.LENGTH_LONG).show();
                } else {
                    btnLogin.setClickable(false);
                    LoginUser loginUser = new LoginUser();
                    loginUser.setEmp_id(etEmail.getText().toString());
                    loginUser.setPassword(etPassword.getText().toString());
                    RestClientRequests.loginUser(loginUser, new LoginResultInterface() {
                        @Override
                        public void loginPerformed(LoginResponse result) {
                            if (result == null) {
                                Toast.makeText(getActivity(), "Error in login", Toast.LENGTH_LONG).show();
                                btnLogin.setClickable(true);
                            } else {
                                if (result.getData().getLocation() == null) {
                                    btnLogin.setClickable(true);
                                    Toast.makeText(getActivity(), "Login performed but no location found. Please add location first", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getActivity(), "Login performed", Toast.LENGTH_LONG).show();
                                    ((MainActivity) getActivity()).loginPerformed(result);

                                }
                            }
                        }
                    });
                }
            }
        });

    }

    @Override
    public void onRefreshData() {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}
