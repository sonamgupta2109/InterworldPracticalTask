package com.example.jiffy03.interworldpracticaltask.serverCalls;

/**
 * Created by Sonam gupta on 15-12-2017.
 */

public interface RestApiConstants {

    String BASE_URL = "http://202.157.171.134/";
    String SEARCH_OCCURENCE = "~smsinvestigation/test/public/api/v1/occurence";
    String LOGIN_USER = "~smsinvestigation/test/public/api/v1/login";
    String ADD_LOCATION = "~smsinvestigation/test/public/api/v1/location/add";
    String EMAIL_ID = "";
    String PASSWORD = "";

}
