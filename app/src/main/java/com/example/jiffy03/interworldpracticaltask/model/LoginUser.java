package com.example.jiffy03.interworldpracticaltask.model;

/**
 * Created by Sonam gupta on 15-12-2017.
 */

public class LoginUser {
    private String emp_id;
    private String password;

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

