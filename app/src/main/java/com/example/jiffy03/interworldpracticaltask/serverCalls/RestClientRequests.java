package com.example.jiffy03.interworldpracticaltask.serverCalls;

import com.example.jiffy03.interworldpracticaltask.interfaces.LoginResultInterface;
import com.example.jiffy03.interworldpracticaltask.interfaces.UpdateBookingInterface;
import com.example.jiffy03.interworldpracticaltask.model.BookingModel;
import com.example.jiffy03.interworldpracticaltask.model.LoginResponse;
import com.example.jiffy03.interworldpracticaltask.model.LoginUser;
import com.example.jiffy03.interworldpracticaltask.utils.Lg;

import retrofit2.Call;
import retrofit2.Callback;

import static com.example.jiffy03.interworldpracticaltask.serverCalls.RestApiConstants.BASE_URL;

/**
 * Created by Sonam gupta on 15-12-2017.
 */

public class RestClientRequests {
    //http://square.github.io/retrofit/

    private static final String TAG = RestClientRequests.class.getSimpleName();

    public static void initRestClient() {
        RestClientBuilder.setup(BASE_URL);
    }

    /**
     * Login user and get token
     */
    public static void loginUser(LoginUser loginUser, final LoginResultInterface loginResultInterface) {
        try {

            retrofit2.Call<LoginResponse> call = RestClientBuilder.get().loginUser(loginUser);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                    Lg.d(TAG, response.toString());
                    if(response.isSuccessful()){
                        if (response.body().getStatus().equalsIgnoreCase("success"))
                            loginResultInterface.loginPerformed(response.body());
                        else
                            loginResultInterface.loginPerformed(null);
                    }else {
                        loginResultInterface.loginPerformed(null);
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Lg.d(TAG, call.toString());
                    loginResultInterface.loginPerformed(null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * save check-in and check-out date on server
     */

    public static void saveBooking(BookingModel bookingModel, final UpdateBookingInterface updateBookingInterface) {
        try {

            retrofit2.Call<Object> call = RestClientBuilder.get().searchOccurrence(bookingModel);
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, retrofit2.Response<Object> response) {
                    Lg.d(TAG, response.toString());
                    if (response.isSuccessful())
                        updateBookingInterface.updateBooking(true);
                    else
                        updateBookingInterface.updateBooking(false);
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Lg.d(TAG, call.toString());
                    updateBookingInterface.updateBooking(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
