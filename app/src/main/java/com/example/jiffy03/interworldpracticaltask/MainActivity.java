package com.example.jiffy03.interworldpracticaltask;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.jiffy03.interworldpracticaltask.fragments.LoginFragment;
import com.example.jiffy03.interworldpracticaltask.interfaces.UpdateBookingInterface;
import com.example.jiffy03.interworldpracticaltask.model.BookingModel;
import com.example.jiffy03.interworldpracticaltask.model.LoginResponse;
import com.example.jiffy03.interworldpracticaltask.serverCalls.RestClientRequests;
import com.example.jiffy03.interworldpracticaltask.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by Sonam gupta on 15-12-2017.
 */
public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener {

    Location myLocation;
    LocationRequest mLocationRequest;
    private String TAG = "MainActivity";
    private LoginResponse loginData;
    private GoogleApiClient mGoogleApiClient;
    private int REQUESTED_PERMISSION_CODE = 100;
    private LoginFragment loginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //startService(new Intent(this, MyService.class));
        setContentView(R.layout.activity_main);
        initApis();
        loginFragment = new LoginFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, loginFragment, "LoginFragment").commit();
    }

    public void initApis() {
        // Create a GoogleApiClient instance
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(new Scope(Scopes.EMAIL))
                .build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }


    @Override
    protected void onStart() {
        super.onStart();
        //check google api is already connected or not
        if (mGoogleApiClient.isConnected()) {
            checkConnection();
        } else {
            mGoogleApiClient.connect();
        }

    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUESTED_PERMISSION_CODE) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkConnection();
            } else {
                // Permission was denied or request was cancelled
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("MainActivity", connectionResult.toString());

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("MainActivity", "ApiClient: OnConnected");
        checkConnection();


    }

    public void checkConnection() {
        // check if permission is granted
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // request Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUESTED_PERMISSION_CODE);
        } else {
            // permission has been granted, continue as usual
            //check location is enable or not
            if (Constants.isLocationEnabled(MainActivity.this)) {
                startLocationUpdates();
            } else {
                // show dialog if location is not enavble
                Constants.enableLocationDialog(MainActivity.this);
            }

        }
    }

    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("MainActivity", i + "");

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(Location location) {
        myLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    public void loginPerformed(LoginResponse loginData) {
        this.loginData = loginData;
        getSupportFragmentManager().beginTransaction().remove(loginFragment).commit();
        checkUserIsInRadius();
    }

    public void checkUserIsInRadius() {
        float[] results = new float[1];
        Location.distanceBetween(myLocation.getLatitude(), myLocation.getLongitude(), Double.parseDouble(loginData.getData().getLocation().get(0).getLatitude()), Double.parseDouble(loginData.getData().getLocation().get(0).getLongitude()), results);
        float distanceInMeters = results[0];
        // Suppose radius is in kilometer
        boolean isWithinRadius = distanceInMeters < Double.parseDouble(loginData.getData().getLocation().get(0).getRadius())*1000;
        if (isWithinRadius) {
            addBookingDate("check-in");
        } else {
            addBookingDate("check-out");
        }

    }

    public void addBookingDate(final String type) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String formattedDate = sdf.format(c.getTime());
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss", Locale.US);
        String formattedTime = sdfTime.format(c.getTime());

        BookingModel bookingModel = new BookingModel();
        bookingModel.setBook_time(formattedTime);
        bookingModel.setBookDate(formattedDate);
        bookingModel.setMessage("Test message");
        bookingModel.setToken(loginData.getData().getToken());
        bookingModel.setLatitude(loginData.getData().getLocation().get(0).getLatitude());
        bookingModel.setLongitude(loginData.getData().getLocation().get(0).getLongitude());
        bookingModel.setCheckInType(type);
        RestClientRequests.saveBooking(bookingModel, new UpdateBookingInterface() {
            @Override
            public void updateBooking(boolean success) {
                if (success){
                    if(type.equals("check-in"))
                        Toast.makeText(MainActivity.this, "User is in location "+type + " data updated", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(MainActivity.this, "User is not in location "+type + " data updated", Toast.LENGTH_LONG).show();
                }
                else
                    Toast.makeText(MainActivity.this, "error in update data", Toast.LENGTH_LONG).show();
            }
        });
    }


}
