package com.example.jiffy03.interworldpracticaltask.interfaces;

import com.example.jiffy03.interworldpracticaltask.model.LoginResponse;

/**
 * Created by Sonam gupta on 16-12-2017.
 */

public interface LoginResultInterface {
    public void loginPerformed(LoginResponse result);
}
