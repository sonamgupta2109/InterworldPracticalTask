package com.example.jiffy03.interworldpracticaltask.serverCalls;

/**
 * Created by Sonam gupta on 15-12-2017.
 */


import com.example.jiffy03.interworldpracticaltask.model.BookingModel;
import com.example.jiffy03.interworldpracticaltask.model.LoginResponse;
import com.example.jiffy03.interworldpracticaltask.model.LoginUser;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

interface RestApiInterface {

    @Headers({"Content-Type:application/json", "Accept:application/json"})
    @POST(RestApiConstants.LOGIN_USER)
    Call<LoginResponse> loginUser(@Body LoginUser loginUser);

    @Headers({"Content-Type:application/json", "Accept:application/json"})
    @POST(RestApiConstants.SEARCH_OCCURENCE)
    Call<Object> searchOccurrence(@Body BookingModel bookingModel);

}
