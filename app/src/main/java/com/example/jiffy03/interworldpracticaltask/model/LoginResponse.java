package com.example.jiffy03.interworldpracticaltask.model;

import java.util.ArrayList;

/**
 * Created by Sonam gupta on 16-12-2017.
 */

public class LoginResponse {

    private String status;
    private Data data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data {
        private String token;
        private String employee_name;
        private ArrayList<Location> location;

        public String getToken() {
            return token;
        }

        public String getEmployee_name() {
            return employee_name;
        }

        public ArrayList<Location> getLocation() {
            return location;
        }
    }

    public class Location {
        private String id;
        private String name;
        private String radius;
        private String longitude;
        private String latitude;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRadius() {
            return radius;
        }

        public void setRadius(String radius) {
            this.radius = radius;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

    }
}

