package com.example.jiffy03.interworldpracticaltask.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by Sonam gupta on 16-12-2017.
 */

public abstract class RootFragment extends Fragment {
    private static final String TAG = RootFragment.class.getSimpleName();

    public abstract void onRefreshData();

    public abstract boolean onBackPressed();

    @Override
    public void onStop() {
        super.onStop();
    }

}
