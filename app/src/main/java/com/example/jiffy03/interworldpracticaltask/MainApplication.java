package com.example.jiffy03.interworldpracticaltask;

import android.app.Application;
import android.content.Context;

import com.example.jiffy03.interworldpracticaltask.serverCalls.RestClientRequests;

/**
 * Created by Sonam gupta on 15-12-2017.
 */

public class MainApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        initRestClient();

    }

    /**
     * init rest client library
     */
    private void initRestClient() {
        RestClientRequests.initRestClient();
    }


}

