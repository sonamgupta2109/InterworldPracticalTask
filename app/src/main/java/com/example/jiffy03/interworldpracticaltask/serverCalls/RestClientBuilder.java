package com.example.jiffy03.interworldpracticaltask.serverCalls;


import com.example.jiffy03.interworldpracticaltask.utils.Lg;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sonam gupta on 15-12-2017.
 */

class RestClientBuilder {
    private static final String TAG = RestClientBuilder.class.getName();
    private static final int TIME_OUT = 10000;
    private static String SERVER_URL = "";
    private static RestApiInterface restApiInterface;

    private RestClientBuilder() {
    }

    public static RestApiInterface get() throws Exception {
        if (restApiInterface == null) {
            Lg.e("call setup(Context) first");
        }
        return restApiInterface;
    }

    public static void setup(String baseUrl) {
        SERVER_URL = baseUrl;


        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        builder.readTimeout(60 * 1000, TimeUnit.MILLISECONDS);
        builder.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                request = request.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .url(request.url())
                        .build();

                Response response = chain.proceed(request);
                handleErrorManually(response);
                return response;
            }
        });
        OkHttpClient client = builder.build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(SERVER_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build();

        restApiInterface = retrofit.create(RestApiInterface.class);

    }

    public static Response handleErrorManually(Response cause) {
        return cause;
    }

}
