package com.example.jiffy03.interworldpracticaltask.interfaces;

/**
 * Created by Sonam gupta on 16-12-2017.
 */

public interface UpdateBookingInterface {

    public void updateBooking(boolean success);
}
